class Connection:
    dp_ts: int
    ar_ts: int
    dp_stop_id: int
    ar_stop_id: int
    trip_id: int
    is_regio: bool
    dist_traveled: int
    dp_platform_id: int
    ar_platform_id: int

class Connections:
    def __init__(self) -> None: ...
    def __len__(self) -> int: ...
    def get_min_dp_ts(self) -> int: ...
    def append(
        self,
        dp_ts: int,
        ar_ts: int,
        dp_stop_id: int,
        ar_stop_id: int,
        trip_id: int,
        is_regio: bool,
        dist_traveled: int,
        dp_platform_id: int,
        ar_platform_id: int,
    ) -> None: ...
    def extend(self, other: Connections) -> None: ...

class Heuristics:
    def __init__(self) -> None: ...
    def insert(self, stop_id: int, heuristic: int) -> None: ...
    def get(self, stop_id: int) -> int: ...

class RoutingProfile:
    Normal: RoutingProfile
    Alternatives: RoutingProfile

class Reachability:
    def __init__(
        self,
        dp_ts: int,
        ar_ts: int,
        changeovers: int,
        dist_traveled: int,
        is_regio: bool,
        current_trip_id: int,
        min_heuristic: int,
        last_id: int,
        last_dp_ts: int,
        last_changeover_duration: int,
        transfer_time_from_delayed_trip: int,
        from_failed_transfer_stop_id: bool,
    ) -> None: ...

class Csa:
    finished: bool

    def __init__(transfers: Transfers, heuristics: Heuristics, destination_stop_id: int) -> None: ...

    def configure(
            self,
            stop_reachability: dict[int, Reachability],
            profile: RoutingProfile,
            delayed_trip_id: int,
            min_delay: int,
    ) -> None: ...

    def extend_connections(self, connections: Connections) -> None: ...

    def run(self, start_ts: int) -> None: ...

    def extract_journeys(self) -> list[list[Connection]]: ...

class Transfers:
    def __init__(self) -> None: ...
    def insert(self, from_stop_id: int, to_stop_id: int, min_transfer_time: int, distance: int) -> None: ...