use crate::constants::*;
use crate::reachability::*;
use pyo3::prelude::*;

#[pyclass(eq, eq_int)]
#[derive(Clone, PartialEq)]
pub enum RoutingProfile {
    Normal,
    Alternatives,
}

fn early_stopping_ts_normal(reachabilities: Vec<Reachability>) -> Option<u64> {
    // Stopping criteria: Find N_ROUTES_TO_FIND and then stop
    if reachabilities.len() >= N_ROUTES_TO_FIND {
        // Find the N_ROUTES_TO_FIND-th minimal arrival time
        let mut ar_times: Vec<u64> = reachabilities.iter().map(|r| r.ar_ts).collect();
        ar_times.sort();

        Some(ar_times[N_ROUTES_TO_FIND - 1])
    } else {
        None
    }
}

fn early_stopping_ts_alternatives(reachabilities: Vec<Reachability>) -> Option<u64> {
    // Stopping criteria: A route from the failed transfer stop was found
    // that has at least as much transfer time as the maximum expected delay.
    if reachabilities.iter().any(|r| {
        r.from_failed_transfer_stop_id
            && r.transfer_time_from_delayed_trip == MAX_EXPECTED_DELAY_SECONDS
    }) {
        Some(reachabilities.iter().map(|r| r.ar_ts).max().unwrap())
    } else {
        None
    }
}

impl RoutingProfile {
    pub fn get_domination_criteria(&self) -> Vec<fn(&Reachability, &Reachability) -> Dominance> {
        match self {
            RoutingProfile::Normal => vec![
                Reachability::dp_ts_dominance,
                Reachability::ar_ts_dominance,
                Reachability::changeovers_dominance,
                Reachability::dist_traveled_dominance,
                Reachability::is_regio_dominance,
                Reachability::last_changeover_duration_dominance,
            ],
            RoutingProfile::Alternatives => vec![
                Reachability::ar_ts_dominance,
                Reachability::changeovers_dominance,
                Reachability::is_regio_dominance,
                Reachability::dist_traveled_dominance,
                Reachability::transfer_time_from_delayed_trip_dominance,
                Reachability::is_from_failed_transfer_stop_id_dominance,
                Reachability::last_changeover_duration_dominance,
            ],
        }
    }

    pub fn get_early_stopping_ts(&self, reachabilities: Vec<Reachability>) -> Option<u64> {
        match self {
            RoutingProfile::Normal => early_stopping_ts_normal(reachabilities),
            RoutingProfile::Alternatives => early_stopping_ts_alternatives(reachabilities),
        }
    }
}
