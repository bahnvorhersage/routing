use crate::constants::{NO_STOP_ID, NO_TRIP_ID, WALKING_TRIP_ID};
use crate::errors::RoutingError;
use crate::reachability::Reachability;
use crate::stops::Stops;
use crate::{connections::*, transfers};
use itertools::Itertools;

pub fn extract_reachability_chains(
    stops: &Stops,
    destination_stop_id: i64,
    early_stopping_ts: Option<u64>,
    reachabilities_start_id: usize,
) -> Result<Vec<Vec<Reachability>>, RoutingError> {
    let mut chains: Vec<Vec<Reachability>> = Vec::new();

    if let Some(dest_reachabilities) = stops.get_reachabilities(destination_stop_id) {
        for dest_reachability in dest_reachabilities
            .iter()
            .filter(|r| match early_stopping_ts {
                Some(ts) => r.ar_ts <= ts,
                None => true,
            })
        {
            let mut chain: Vec<Reachability> = Vec::new();
            let mut reachability = dest_reachability;
            loop {
                chain.push(*reachability);
                if reachability.last_id <= reachabilities_start_id {
                    break;
                }
                reachability = stops
                    .get_reachability(reachability.last_id)
                    .ok_or(RoutingError::ReachabilityNotFound(reachability.last_id))?;
            }
            chains.push(chain);
        }
    }
    Ok(chains)
}

pub fn reachability_chains_to_journeys(
    chains: Vec<Vec<Reachability>>,
    connections: &Connections,
    transfers: &transfers::Transfers,
    origin_stop_ids: &Vec<i64>,
    destination_stop_id: i64,
) -> Result<Vec<Vec<Connection>>, RoutingError> {
    let mut journeys = Vec::new();
    for chain in chains.iter() {
        let mut journey = Vec::new();
        for reachability in chain.iter().rev() {
            if reachability.current_trip_id == NO_TRIP_ID {
                continue;
            }
            let mut connection = connections
                .get_by(reachability.last_dp_ts, reachability.current_trip_id)
                .ok_or(RoutingError::ConnectionNotFound {
                    earliest_dp_ts: reachability.last_dp_ts,
                    trip_id: reachability.current_trip_id,
                })?;
            journey.push(*connection);
            while connection.ar_ts < reachability.ar_ts {
                connection = connections
                    .get_by(connection.ar_ts, connection.trip_id)
                    .ok_or(RoutingError::ConnectionNotFound {
                        earliest_dp_ts: connection.ar_ts,
                        trip_id: connection.trip_id,
                    })?;
                journey.push(*connection);
            }
        }
        journey = insert_transfers(&journey, transfers, origin_stop_ids, destination_stop_id)?;
        journeys.push(journey);
    }
    Ok(journeys)
}

enum TransferTs {
    StartTransferAt(u64),
    FinishTransferUntil(u64),
}

fn get_transfer_connection(
    transfers: &transfers::Transfers,
    dp_stop_id: i64,
    ar_stop_id: i64,
    transfer_ts: TransferTs,
) -> Result<Connection, RoutingError> {
    let transfer =
        transfers
            .get_exact(dp_stop_id, ar_stop_id)
            .ok_or(RoutingError::TransferNotFound {
                from_stop_id: dp_stop_id,
                to_stop_id: ar_stop_id,
            })?;
    let (dp_ts, ar_ts) = match transfer_ts {
        TransferTs::StartTransferAt(ts) => (ts, ts + transfer.min_transfer_time),
        TransferTs::FinishTransferUntil(ts) => (ts - transfer.min_transfer_time, ts),
    };
    Ok(Connection {
        dp_ts,
        ar_ts,
        dp_stop_id,
        ar_stop_id,
        trip_id: WALKING_TRIP_ID,
        is_regio: true,
        dist_traveled: 0,
        dp_platform_id: NO_STOP_ID,
        ar_platform_id: NO_STOP_ID,
    })
}

pub fn insert_transfers(
    journey: &[Connection],
    transfers: &transfers::Transfers,
    origin_stop_ids: &[i64],
    destination_stop_id: i64,
) -> Result<Vec<Connection>, RoutingError> {
    let mut new_journey = Vec::new();

    if !origin_stop_ids.contains(&journey[0].dp_stop_id) {
        let transfer_connection = origin_stop_ids.iter().find_map(|stop_id| {
            get_transfer_connection(
                transfers,
                *stop_id,
                journey[0].dp_stop_id,
                TransferTs::FinishTransferUntil(journey[0].dp_ts),
            )
            .ok()
        });

        match transfer_connection {
            Some(transfer_connection) => new_journey.push(transfer_connection),
            None => {
                return Err(RoutingError::TransferNotFound {
                    from_stop_id: origin_stop_ids[0],
                    to_stop_id: journey[0].dp_stop_id,
                })
            }
        }

        // // If journey does not start at origin stop, insert transfer
    }

    for (c1, c2) in journey.iter().tuple_windows() {
        new_journey.push(*c1);
        if c1.trip_id != c2.trip_id && c1.ar_stop_id != c2.dp_stop_id {
            // If there is a transfer between two connections, insert it
            let transfer_connection = get_transfer_connection(
                transfers,
                c1.ar_stop_id,
                c2.dp_stop_id,
                TransferTs::StartTransferAt(c1.ar_ts),
            )?;
            new_journey.push(transfer_connection);
        }
    }

    new_journey.push(journey[journey.len() - 1]);

    if destination_stop_id != journey[journey.len() - 1].ar_stop_id {
        // If journey does not end at destination stop, insert transfer
        let transfer_connection = get_transfer_connection(
            transfers,
            journey[journey.len() - 1].ar_stop_id,
            destination_stop_id,
            TransferTs::StartTransferAt(journey[journey.len() - 1].ar_ts),
        )?;
        new_journey.push(transfer_connection);
    }

    Ok(new_journey)
}
