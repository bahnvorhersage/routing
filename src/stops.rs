use crate::profiles::*;
use crate::reachability::*;
use std::collections::HashMap;

pub struct Stops {
    reachabilities: Vec<Reachability>,
    stop_reachabilities_id: HashMap<i64, Vec<usize>>,
}

pub enum InsertIndex {
    Inserted(usize),
    Dominated,
}

impl Stops {
    pub fn new() -> Stops {
        Stops {
            reachabilities: Vec::new(),
            stop_reachabilities_id: HashMap::new(),
        }
    }

    pub fn insert_no_check(&mut self, stop_id: i64, reachability: Reachability) -> usize {
        let id = self.reachabilities.len();
        self.reachabilities.push(reachability);

        match self.stop_reachabilities_id.get_mut(&stop_id) {
            None => {
                self.stop_reachabilities_id.insert(stop_id, vec![id]);
            }
            Some(reachabilities) => {
                reachabilities.push(id);
            }
        }

        id
    }

    pub fn insert_reachability(
        &mut self,
        stop_id: i64,
        reachability: Reachability,
        profile: &RoutingProfile,
    ) -> InsertIndex {
        let id = self.reachabilities.len();

        let inserted = match self.stop_reachabilities_id.get_mut(&stop_id) {
            None => {
                self.stop_reachabilities_id.insert(stop_id, vec![id]);
                InsertIndex::Inserted(id)
            }
            Some(ids) => {
                let dominated = ids.iter().rev().any(|&other_id| {
                    reachability.is_dominated_by(&self.reachabilities[other_id], profile)
                });

                if dominated {
                    InsertIndex::Dominated
                } else {
                    ids.retain(|&other_id| {
                        !self.reachabilities[other_id].is_dominated_by(&reachability, profile)
                    });
                    ids.push(id);
                    InsertIndex::Inserted(id)
                }
            }
        };

        match inserted {
            InsertIndex::Inserted(id) => {
                self.reachabilities.push(reachability);
                InsertIndex::Inserted(id)
            }
            InsertIndex::Dominated => InsertIndex::Dominated,
        }
    }

    pub fn get_reachabilities(&self, stop_id: i64) -> Option<Vec<Reachability>> {
        let ids = self.stop_reachabilities_id.get(&stop_id)?;
        Some(ids.iter().map(|&id| self.reachabilities[id]).collect())
    }

    pub fn get_reachability_ids(&self, stop_id: i64) -> Option<&Vec<usize>> {
        self.stop_reachabilities_id.get(&stop_id)
    }

    pub fn get_reachability(&self, id: usize) -> Option<&Reachability> {
        self.reachabilities.get(id)
    }
}
