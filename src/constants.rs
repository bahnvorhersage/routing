pub const MAX_METERS_DRIVING_AWAY: u32 = 30_000;
pub const EXTRA_DURATION_FOR_SHORTER_ROUTE: f32 = 1.1; // 10% longer duration for a shorter route (geo-distance) is acceptable

pub const NO_TRIP_ID: i64 = 0;
pub const NO_DELAYED_TRIP_ID: i64 = 1;
pub const WALKING_TRIP_ID: i64 = 2;

pub const NO_STOP_ID: i64 = 0;
pub const MAX_EXPECTED_DELAY_SECONDS: u64 = 60 * 30;
pub const MINIMAL_DISTANCE_DIFFERENCE: u32 = 1000; // 1 km
pub const N_ROUTES_TO_FIND: usize = 14;
