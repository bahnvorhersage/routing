use pyo3::prelude::*;
use std::collections::HashMap;

#[pyclass]
#[derive(Clone)]
pub struct Heuristics {
    heuristic: HashMap<i64, u32>,
}

#[pymethods]
impl Heuristics {
    #[new]
    pub fn new() -> Heuristics {
        Heuristics {
            heuristic: HashMap::new(),
        }
    }

    fn insert(&mut self, stop_id: i64, heuristic: u32) {
        self.heuristic.insert(stop_id, heuristic);
    }

    pub fn get(&self, stop_id: i64) -> u32 {
        match self.heuristic.get(&stop_id) {
            Some(heuristic) => *heuristic,
            None => 0,
        }
    }
}
