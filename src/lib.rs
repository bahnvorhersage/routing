mod connections;
mod constants;
mod errors;
mod heuristics;
mod journey_extraction;
mod profiles;
mod reachability;
mod routing;
mod stops;
mod transfers;
mod trips;

use connections::Connection;
use connections::Connections;
use heuristics::Heuristics;
use pyo3::prelude::*;
use reachability::Reachability;
use routing::Csa;
use transfers::Transfers;
use profiles::RoutingProfile;

/// A Python module implemented in Rust.
#[pymodule]
fn bahnvorhersage_routing(_py: Python, m: &Bound<'_, PyModule>) -> PyResult<()> {
    m.add_class::<Connection>()?;
    m.add_class::<Connections>()?;
    m.add_class::<Heuristics>()?;
    m.add_class::<Transfers>()?;
    m.add_class::<Reachability>()?;
    m.add_class::<RoutingProfile>()?;
    m.add_class::<Csa>()?;

    // m.add_function(wrap_pyfunction!(csa, m)?)?;

    Ok(())
}
