use bisection::bisect_left_by;
use pyo3::prelude::*;

// Direct connection from one stop to the next without stopovers
#[pyclass]
#[derive(Clone, Copy, Debug)]
pub struct Connection {
    #[pyo3(get)]
    pub dp_ts: u64,
    #[pyo3(get)]
    pub ar_ts: u64,
    #[pyo3(get)]
    pub dp_stop_id: i64,
    #[pyo3(get)]
    pub ar_stop_id: i64,
    #[pyo3(get)]
    pub trip_id: i64,
    #[pyo3(get)]
    pub is_regio: bool,
    #[pyo3(get)]
    pub dist_traveled: u32,
    #[pyo3(get)]
    pub dp_platform_id: i64,
    #[pyo3(get)]
    pub ar_platform_id: i64,
}

#[pyclass]
pub struct Connections {
    pub connections: Vec<Connection>,
}

#[pymethods]
impl Connections {
    #[new]
    pub fn new() -> Connections {
        Connections {
            connections: Vec::new(),
        }
    }

    pub fn __len__(&self) -> usize {
        self.connections.len()
    }

    pub fn get_min_dp_ts(&self) -> u64 {
        self.connections[0].dp_ts
    }

    pub fn append(
        &mut self,
        dp_ts: u64,
        ar_ts: u64,
        dp_stop_id: i64,
        ar_stop_id: i64,
        trip_id: i64,
        is_regio: bool,
        dist_traveled: u32,
        dp_platform_id: i64,
        ar_platform_id: i64,
    ) {
        let connection = Connection {
            dp_ts,
            ar_ts,
            dp_stop_id,
            ar_stop_id,
            trip_id,
            is_regio,
            dist_traveled,
            dp_platform_id,
            ar_platform_id,
        };
        self.connections.push(connection);
    }

    pub fn extend(&mut self, other: &Connections) {
        self.connections.extend_from_slice(&other.connections);
    }
}

impl Connections {
    pub fn get_by(&self, dp_ts: u64, trip_id: i64) -> Option<&Connection> {
        let index = bisect_left_by(&self.connections, |c| c.dp_ts.cmp(&dp_ts));
        if index < self.connections.len() {
            for i in index..self.connections.len() {
                if self.connections[i].trip_id == trip_id {
                    return Some(&self.connections[i]);
                }
            }
        } else {
            println!("Index out of bounds");
        }
        None
    }
}
