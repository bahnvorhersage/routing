use crate::connections::*;
use crate::constants::*;
use crate::reachability::*;
use crate::profiles::*;
use pyo3::prelude::*;
use std::collections::HashMap;

#[pyclass]
pub struct Trips {
    pub trips: HashMap<i64, Vec<Reachability>>,
}

#[pymethods]
impl Trips {
    #[new]
    pub fn new() -> Trips {
        Trips {
            trips: HashMap::new(),
        }
    }

    pub fn insert_no_check(&mut self, reachability: Reachability) {
        match self.trips.get_mut(&reachability.current_trip_id) {
            None => {
                self.trips.insert(reachability.current_trip_id, vec![reachability]);
            }
            Some(reachabilities) => {
                reachabilities.push(reachability);
            }
        }
    }
}

impl Trips {
    pub fn insert_reachability(
        &mut self,
        trip_id: i64,
        reachability: Reachability,
        profile: &RoutingProfile,
    ) -> bool {
        match self.trips.get_mut(&trip_id) {
            None => {
                self.trips.insert(trip_id, vec![reachability]);
                true
            }
            Some(reachabilities) => {
                let dominated = reachabilities
                    .iter()
                    .rev()
                    .any(|r| reachability.is_dominated_by(r, profile));

                if dominated {
                    false
                } else {
                    reachabilities
                        .retain(|r| !r.is_dominated_by(&reachability, profile));
                    reachabilities.push(reachability);
                    true
                }
            }
        }
    }

    pub fn add_connection(&mut self, trip_id: i64, connection: &Connection, heuristic: u32) {
        match self.trips.get_mut(&trip_id) {
            None => {}
            Some(reachabilities) => {
                reachabilities.retain(|reachability| {
                    heuristic < (MAX_METERS_DRIVING_AWAY + reachability.min_heuristic)
                });

                reachabilities.iter_mut().for_each(|reachability| {
                    reachability.add_connection_to_trip_reachability(connection, heuristic);
                });
            }
        }
    }

    pub fn get(&self, trip_id: i64) -> Option<&Vec<Reachability>> {
        self.trips.get(&trip_id)
    }
}
