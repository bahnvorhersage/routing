use pyo3::prelude::*;
use std::collections::HashMap;

#[derive(Clone, Debug)]
pub struct Transfer {
    pub from_stop_id: i64,
    pub to_stop_id: i64,
    pub min_transfer_time: u64,
    pub distance: u32,
}

#[pyclass]
#[derive(Clone)]
pub struct Transfers {
    transfers: HashMap<i64, Vec<Transfer>>,
}

#[pymethods]
impl Transfers {
    #[new]
    pub fn new() -> Transfers {
        Transfers {
            transfers: HashMap::new(),
        }
    }

    pub fn insert(
        &mut self,
        from_stop_id: i64,
        to_stop_id: i64,
        min_transfer_time: u64,
        distance: u32,
    ) {
        let transfer = Transfer {
            from_stop_id,
            to_stop_id,
            min_transfer_time,
            distance,
        };
        match self.transfers.get_mut(&from_stop_id) {
            Some(transfers) => transfers.push(transfer),
            None => {
                self.transfers.insert(from_stop_id, vec![transfer]);
            }
        }
    }
}

impl Transfers {
    pub fn get(&self, from_stop_id: i64) -> Option<&Vec<Transfer>> {
        self.transfers.get(&from_stop_id)
    }

    pub fn get_exact(&self, from_stop_id: i64, to_stop_id: i64) -> Option<&Transfer> {
        match self.transfers.get(&from_stop_id) {
            Some(transfers) => {
                for transfer in transfers.iter() {
                    if transfer.to_stop_id == to_stop_id {
                        return Some(transfer);
                    }
                }
                None
            }
            None => None,
        }
    }
}
