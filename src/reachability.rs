use crate::connections::*;
use crate::constants::*;
use crate::transfers::*;
use crate::profiles::*;
use chrono::DateTime;
use pyo3::prelude::*;
use std::cmp::min;
use std::fmt;
use std::cmp::Ordering;

pub enum Dominance {
    Worse,
    Equal,
    Better,
}

#[pyclass]
#[derive(Copy, Clone)]
pub struct Reachability {
    pub dp_ts: u64,
    pub ar_ts: u64,
    pub changeovers: u8,
    pub dist_traveled: u32,
    pub is_regio: bool,
    pub current_trip_id: i64,
    pub min_heuristic: u32,
    pub last_id: usize,
    pub last_dp_ts: u64,
    pub last_changeover_duration: u64,
    pub transfer_time_from_delayed_trip: u64,
    pub from_failed_transfer_stop_id: bool,
}

impl fmt::Debug for Reachability {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt.debug_struct("Reachability")
            .field(
                "dp_ts",
                &format_args!(
                    "{}",
                    DateTime::from_timestamp(self.dp_ts as i64, 0)
                        .unwrap()
                        .format("%H:%M")
                ),
            )
            .field(
                "ar_ts",
                &format_args!(
                    "{}",
                    DateTime::from_timestamp(self.ar_ts as i64, 0)
                        .unwrap()
                        .format("%H:%M")
                ),
            )
            .field("changeovers", &self.changeovers)
            .field(
                "dist_traveled",
                &format_args!("{:.2} km", self.dist_traveled as f32 / 1000.0),
            )
            .field("is_regio", &self.is_regio)
            .field("current_trip_id", &self.current_trip_id)
            .field("min_heuristic", &self.min_heuristic)
            // .field("id", &self.id)
            .field("last_id", &self.last_id)
            // .field("last_stop_id", &self.last_stop_id)
            .field(
                "last_dp_ts",
                &format_args!(
                    "{}",
                    DateTime::from_timestamp(self.last_dp_ts as i64, 0)
                        .unwrap()
                        .format("%H:%M")
                ),
            )
            .field(
                "last_changeover_duration",
                &format_args!("{:.2} min", self.last_changeover_duration as f32 / 60.0),
            )
            .field(
                "transfer_time_from_delayed_trip",
                &format_args!(
                    "{:.2} min",
                    self.transfer_time_from_delayed_trip as f32 / 60.0
                ),
            )
            .field(
                "from_failed_transfer_stop_id",
                &self.from_failed_transfer_stop_id,
            )
            .finish()
    }
}

#[pymethods]
impl Reachability {
    #[new]
    fn new(
        dp_ts: u64,
        ar_ts: u64,
        changeovers: u8,
        dist_traveled: u32,
        is_regio: bool,
        current_trip_id: i64,
        min_heuristic: u32,
        last_id: usize,
        last_dp_ts: u64,
        last_changeover_duration: u64,
        transfer_time_from_delayed_trip: u64,
        from_failed_transfer_stop_id: bool,
    ) -> Self {
        Reachability {
            dp_ts,
            ar_ts,
            changeovers,
            dist_traveled,
            is_regio,
            current_trip_id,
            min_heuristic,
            last_id,
            last_dp_ts,
            last_changeover_duration,
            transfer_time_from_delayed_trip,
            from_failed_transfer_stop_id,
        }
    }
}

impl Reachability {
    pub fn from_previous_and_connection(
        previous: &Reachability,
        last_id: usize,
        connection: &Connection,
        transfer: &Transfer,
        transfer_time_from_delayed_trip: u64,
        min_heuristic: u32,
    ) -> Self {
        let dp_ts = if previous.current_trip_id != NO_TRIP_ID {
            previous.dp_ts
        } else if transfer.from_stop_id != transfer.to_stop_id {
            connection.dp_ts - transfer.min_transfer_time
        } else {
            connection.dp_ts
        };
        let changeovers = if previous.current_trip_id == NO_TRIP_ID {
            previous.changeovers
        } else {
            previous.changeovers + 1
        };

        if (connection.dp_ts as i64 - previous.ar_ts as i64 - transfer.min_transfer_time as i64) < 0
        {
            println!(
                "Negative transfer time: {} - {} - {}",
                connection.dp_ts, previous.ar_ts, transfer.min_transfer_time
            );
        }

        Reachability {
            dp_ts,
            ar_ts: connection.ar_ts,
            changeovers,
            dist_traveled: previous.dist_traveled + connection.dist_traveled + transfer.distance,
            is_regio: previous.is_regio && connection.is_regio,
            current_trip_id: connection.trip_id,
            min_heuristic,
            last_id,                   
            last_dp_ts: connection.dp_ts,
            last_changeover_duration: connection.dp_ts
                - previous.ar_ts
                - transfer.min_transfer_time,
            transfer_time_from_delayed_trip,
            from_failed_transfer_stop_id: previous.from_failed_transfer_stop_id,
        }
    }

    pub fn add_connection_to_trip_reachability(&mut self, connection: &Connection, heuristic: u32) {
        self.ar_ts = connection.ar_ts;
        self.dist_traveled += connection.dist_traveled;
        self.min_heuristic = min(self.min_heuristic, heuristic);
    }

    pub fn dp_ts_dominance(&self, other: &Reachability) -> Dominance {
        match self.dp_ts.cmp(&other.dp_ts) {
            Ordering::Greater => Dominance::Better,
            Ordering::Less => Dominance::Worse,
            Ordering::Equal => Dominance::Equal,
        }
    }

    pub fn ar_ts_dominance(&self, other: &Reachability) -> Dominance {
        match self.ar_ts.cmp(&other.ar_ts) {
            Ordering::Greater => Dominance::Worse,
            Ordering::Less => Dominance::Better,
            Ordering::Equal => Dominance::Equal,
        }
    }

    pub fn changeovers_dominance(&self, other: &Reachability) -> Dominance {
        match self.changeovers.cmp(&other.changeovers) {
            Ordering::Greater => Dominance::Worse,
            Ordering::Less => Dominance::Better,
            Ordering::Equal => Dominance::Equal,
        }
    }

    pub fn dist_traveled_dominance(&self, other: &Reachability) -> Dominance {
        let distance_difference = self.dist_traveled.abs_diff(other.dist_traveled);
        if distance_difference < MINIMAL_DISTANCE_DIFFERENCE {
            Dominance::Equal
        } else if self.dist_traveled < other.dist_traveled {
            let duration = self.ar_ts as f32 - self.dp_ts as f32;
            let other_duration = other.ar_ts as f32 - other.dp_ts as f32;
            if duration > other_duration * EXTRA_DURATION_FOR_SHORTER_ROUTE {
                return Dominance::Equal;
            } else {
                return Dominance::Worse;
            }
        } else {
            Dominance::Worse
        }
    }

    pub fn is_regio_dominance(&self, other: &Reachability) -> Dominance {
        match self.is_regio.cmp(&other.is_regio) {
            Ordering::Greater => Dominance::Better,
            Ordering::Less => Dominance::Worse,
            Ordering::Equal => Dominance::Equal,
        }
    }

    pub fn transfer_time_from_delayed_trip_dominance(&self, other: &Reachability) -> Dominance {
        match self
            .transfer_time_from_delayed_trip
            .cmp(&other.transfer_time_from_delayed_trip)
        {
            Ordering::Greater => Dominance::Better,
            Ordering::Less => Dominance::Worse,
            Ordering::Equal => Dominance::Equal,
        }
    }

    pub fn is_from_failed_transfer_stop_id_dominance(&self, other: &Reachability) -> Dominance {
        match self
            .from_failed_transfer_stop_id
            .cmp(&other.from_failed_transfer_stop_id)
        {
            Ordering::Greater => Dominance::Better,
            Ordering::Less => Dominance::Worse,
            Ordering::Equal => Dominance::Equal,
        }
    }

    pub fn last_changeover_duration_dominance(&self, other: &Reachability) -> Dominance {
        // Last changeover duration is a criteria to sort out reachabilities in the following case:
        // Routing from stop a to stop e:
        // Train 1: 🭃🭎 -> a -> b -> c -> d
        // Train 2:      🭃🭎 -> b -> c -> d -> e
        // Changeovers at b, c and d make sense, so we want to pick the changeover with
        // the longest duration, in order to increase journey reliability.
        // Due to this we only compare reachabilities with the same dp_ts and ar_ts.
        if self.ar_ts == other.ar_ts
            && self.dp_ts == other.dp_ts
            && self.last_changeover_duration < other.last_changeover_duration
        {
            return Dominance::Worse;
        }
        Dominance::Equal
    }

    pub fn is_dominated_by(
        &self,
        other: &Reachability,
        profile: &RoutingProfile,
    ) -> bool {
        let domination_criteria = profile.get_domination_criteria();
        let mut is_worse = false;
        for criterion in domination_criteria {
            match criterion(self, other) {
                Dominance::Better => return false,
                Dominance::Worse => {
                    is_worse = true;
                    continue;
                }
                Dominance::Equal => continue,
            }
        }
        is_worse
    }
}
