use std::cmp::max;
use std::cmp::min;
use std::collections::HashMap;

use crate::connections::*;
use crate::constants::*;
use crate::errors::RoutingError;
use crate::heuristics::*;
use crate::journey_extraction::*;
use crate::profiles::*;
use crate::reachability::*;
use crate::stops::*;
use crate::transfers::*;
use crate::trips::*;
use bisection::bisect_left_by;
use pyo3::prelude::*;

#[pyclass]
pub struct Csa {
    connections: Connections,
    stops: Stops,
    trips: Trips,
    transfers: Transfers,
    heuristics: Heuristics,
    profile: RoutingProfile,
    reachabilities_start_id: usize,
    origin_stop_ids: Vec<i64>,
    destination_stop_id: i64,
    early_stopping_ts: Option<u64>,
    delayed_trip_id: i64,
    min_delay: u64,
    #[pyo3(get)]
    finished: bool,
}

#[pymethods]
impl Csa {
    #[new]
    fn new(transfers: Transfers, heuristics: Heuristics, destination_stop_id: i64) -> Csa {
        Csa {
            connections: Connections::new(),
            stops: Stops::new(),
            trips: Trips::new(),
            transfers,
            heuristics,
            profile: RoutingProfile::Normal,
            reachabilities_start_id: 0,
            origin_stop_ids: Vec::<i64>::new(),
            destination_stop_id,
            early_stopping_ts: None,
            delayed_trip_id: NO_DELAYED_TRIP_ID,
            min_delay: 0,
            finished: false,
        }
    }

    fn configure(
        &mut self,
        stop_reachabilities: HashMap<i64, Reachability>,
        profile: RoutingProfile,
        delayed_trip_id: i64,
        min_delay: u64,
    ) {
        self.stops = Stops::new();
        self.trips = Trips::new();

        for (stop_id, reachability) in stop_reachabilities.iter() {
            self.stops.insert_no_check(*stop_id, *reachability);
        }
        self.origin_stop_ids = stop_reachabilities.keys().copied().collect();
        self.reachabilities_start_id = self.origin_stop_ids.len() - 1;

        self.profile = profile;
        self.delayed_trip_id = delayed_trip_id;
        self.min_delay = min_delay;
        self.finished = false;
        self.early_stopping_ts = None;
    }

    fn extend_connections(&mut self, connections: &Connections) {
        self.connections.extend(connections);
    }

    fn run(&mut self, start_ts: u64) -> Result<(), RoutingError> {
        let start_index = bisect_left_by(&self.connections.connections, |c| c.dp_ts.cmp(&start_ts));
        for connection in self.connections.connections[start_index..].iter() {
            if let Some(ts) = self.early_stopping_ts {
                if connection.dp_ts > ts {
                    self.finished = true;
                    return Ok(());
                }
            }

            self.trips.add_connection(
                connection.trip_id,
                connection,
                self.heuristics.get(connection.ar_stop_id),
            );

            if let Some(reachabilities) = self.trips.get(connection.trip_id) {
                for trip_reachability in reachabilities.iter() {
                    self.stops.insert_reachability(
                        connection.ar_stop_id,
                        *trip_reachability,
                        &self.profile,
                    );
                }
            }

            if let Some(transfers) = self.transfers.get(connection.dp_stop_id) {
                for transfer in transfers.iter() {
                    let mut new_reachabilities: Vec<Reachability> = Vec::new();

                    if let Some(last_ids) = self.stops.get_reachability_ids(transfer.to_stop_id) {
                        for last_id in last_ids.iter() {
                            let previous = self
                                .stops
                                .get_reachability(*last_id)
                                .ok_or(RoutingError::ReachabilityNotFound(*last_id))?;

                            let heuristic = self.heuristics.get(connection.ar_stop_id);
                            if heuristic > previous.min_heuristic + MAX_METERS_DRIVING_AWAY {
                                continue;
                            }

                            let is_delayed = previous.current_trip_id == self.delayed_trip_id;
                            let transfer_time = if is_delayed {
                                connection.dp_ts as i64
                                    - previous.ar_ts as i64
                                    - transfer.min_transfer_time as i64
                                    - self.min_delay as i64
                            } else {
                                connection.dp_ts as i64
                                    - previous.ar_ts as i64
                                    - transfer.min_transfer_time as i64
                            };
                            let is_same_trip = connection.trip_id == previous.current_trip_id;

                            // Connection is reachable if there is transfer time. If the connection is on the same trip,
                            // it was already handled.
                            if !is_same_trip && transfer_time >= 0 {
                                // Check if the connection is driving away from the destination. If it drives away more than
                                // MAX_METERS_DRIVING_AWAY, skip it.
                                if self.heuristics.get(connection.ar_stop_id)
                                    > previous.min_heuristic + MAX_METERS_DRIVING_AWAY
                                {
                                    continue;
                                }

                                // If the previous trip was delayed, compute the transfer time from the delayed trip.
                                let transfer_time_from_delayed_trip =
                                    if is_delayed && previous.from_failed_transfer_stop_id {
                                        min(
                                            max(
                                                connection.dp_ts as i64
                                                    - previous.ar_ts as i64
                                                    - transfer.min_transfer_time as i64,
                                                0,
                                            ) as u64,
                                            MAX_EXPECTED_DELAY_SECONDS,
                                        )
                                    } else {
                                        previous.transfer_time_from_delayed_trip
                                    };

                                let reachability = Reachability::from_previous_and_connection(
                                    previous,
                                    *last_id,
                                    connection,
                                    transfer,
                                    transfer_time_from_delayed_trip,
                                    self.heuristics.get(connection.ar_stop_id),
                                );

                                new_reachabilities.push(reachability);
                                self.trips.insert_reachability(
                                    connection.trip_id,
                                    reachability,
                                    &self.profile,
                                );
                            }
                        }
                    }
                    for reachability in new_reachabilities.iter() {
                        self.stops.insert_reachability(
                            connection.ar_stop_id,
                            *reachability,
                            &self.profile,
                        );
                    }
                }
            }

            if connection.ar_stop_id == self.destination_stop_id {
                // generate stopping condition for early stopping
                if let Some(reachabilities) =
                    self.stops.get_reachabilities(self.destination_stop_id)
                {
                    self.early_stopping_ts = self.profile.get_early_stopping_ts(reachabilities);
                }
            }
        }
        Ok(())
    }

    fn extract_journeys(&self) -> Result<Vec<Vec<Connection>>, RoutingError> {
        let chains = extract_reachability_chains(
            &self.stops,
            self.destination_stop_id,
            self.early_stopping_ts,
            self.reachabilities_start_id,
        )?;
        reachability_chains_to_journeys(
            chains,
            &self.connections,
            &self.transfers,
            &self.origin_stop_ids,
            self.destination_stop_id,
        )
    }
}
