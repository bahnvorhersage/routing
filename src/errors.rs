use pyo3::exceptions::PyValueError;
use pyo3::prelude::*;

#[derive(Debug)]
pub enum RoutingError {
    TransferNotFound { from_stop_id: i64, to_stop_id: i64 },
    ConnectionNotFound { earliest_dp_ts: u64, trip_id: i64 },
    ReachabilityNotFound(usize),
    NoReachabilitiesReachedStop { stop_id: i64 },
}

impl From<RoutingError> for PyErr {
    fn from(error: RoutingError) -> Self {
        match error {
            RoutingError::TransferNotFound {
                from_stop_id,
                to_stop_id,
            } => PyValueError::new_err(format!(
                "Transfer from stop_id {} to stop_id {} not found",
                from_stop_id, to_stop_id
            )),
            RoutingError::ConnectionNotFound {
                earliest_dp_ts,
                trip_id,
            } => PyValueError::new_err(format!(
                "Connection with earliest departure timestamp {} and trip_id {} not found",
                earliest_dp_ts, trip_id
            )),
            RoutingError::ReachabilityNotFound(id) => {
                PyValueError::new_err(format!("Reachability with id {} not found", id))
            }
            RoutingError::NoReachabilitiesReachedStop { stop_id } => {
                PyValueError::new_err(format!("No reachabilities reached stop_id {}", stop_id))
            }
        }
    }
}
